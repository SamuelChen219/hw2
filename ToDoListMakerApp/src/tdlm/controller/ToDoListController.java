package tdlm.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.Calendar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
import tdlm.data.DataManager;
import tdlm.gui.Workspace;
import saf.AppTemplate;
import saf.controller.AppFileController;
import saf.ui.AppYesNoCancelDialogSingleton;
import tdlm.PropertyType;
import tdlm.data.ToDoItem;
import tdlm.gui.Deletion;
import tdlm.gui.InformationDialog;


/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public class ToDoListController {
    AppTemplate app;
    Calendar local;
   
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processAddItem() {	
	// ENABLE/DISABLE THE PROPER BUTTONS
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
         workspace.reloadWorkspace();
        InformationDialog dialog =  InformationDialog.getSingleton();
        dialog.init(dialog);
        dialog.setStartDate(LocalDate.now());
        dialog.setEndDate(LocalDate.now());
        dialog.Show();
        ToDoItem item = dialog.getItem();
        if(item!=null)
        workspace.add(item);
        dialog.resetItem();
      }      
    
    public void processRemoveItem() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
       Deletion d = Deletion.getSingleton();
	d.init(d);
        d.show(props.getProperty(PropertyType.DELET), props.getProperty(PropertyType.DELETMESSAGE));
        if(d.getOff())
        workspace.removeTable();
         app.getGUI().updateToolbarControls(false);
    }
    
    public void processMoveUpItem() {
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        workspace.moveup();
         app.getGUI().updateToolbarControls(false);
    }
    
    public void processMoveDownItem() {
           Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        workspace.movedown();
         app.getGUI().updateToolbarControls(false);
    }
    
    public void processEditItem() {
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
         workspace.reloadWorkspace();
        workspace.editTable();
    }
   
}
