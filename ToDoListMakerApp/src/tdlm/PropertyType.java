/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm;

/**
 *
 * @author McKillaGorilla
 */
public enum PropertyType {
    WORKSPACE_HEADING_LABEL,
    
    DETAILS_HEADING_LABEL,
    NAME_PROMPT,
    OWNER_PROMPT,

    ITEMS_HEADING_LABEL,
    
    ADD_ICON, 
    ADD_ITEM_TOOLTIP, 
    
    REMOVE_ICON, 
    REMOVE_ITEM_TOOLTIP, 
    
    MOVE_UP_ICON, 
    MOVE_UP_ITEM_TOOLTIP, 
    
    MOVE_DOWN_ICON, 
    MOVE_DOWN_ITEM_TOOLTIP,
    
   
    
    PROPERTIES_,
    
    CATAGORY,
    
    DESCRIPTION,
    
    START_DATE,
    
    END_DATE,
    
    CHECKBOX,
    
    DELETMESSAGE,
    
    DELET,
    
    SAVE,
    
    CANCEL,
    
    NO
    
    
}
