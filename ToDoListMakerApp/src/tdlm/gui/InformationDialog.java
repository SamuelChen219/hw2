/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.gui;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.time.LocalDate;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.ui.AppMessageDialogSingleton;
import tdlm.PropertyType;
import saf.settings.AppPropertyType;
import tdlm.data.DataManager;
import tdlm.data.ToDoItem;

/**
 *
 * @author samuelchen
 */
public class InformationDialog extends Stage{
    AppTemplate app;
    static InformationDialog singleton = null;
     Stage stage;
    GridPane pane;
   public Button save;
    public Button cancel;
    TextField cat ;
    TextField des ;
    DatePicker start ;
    DatePicker end;
    CheckBox ck ;
    Scene scene;
    Label Cat;
    Label Des;
    Label date;
    Label date2;
    Label check;
     static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CSS_PATH = "/css/";
    static final String MINE= "bordered_mine";
    ToDoItem items;
    
     public static InformationDialog getSingleton() {
	if (singleton == null)
	    singleton = new InformationDialog();
	return singleton;
    }
     private InformationDialog() {}
      public void init(Stage owner) {
    PropertiesManager props = PropertiesManager.getPropertiesManager();
       stage = new Stage();
       pane = new GridPane();
        scene = new Scene(pane);
        scene.getStylesheets().add("/tdlm/css/tdlm_style.css");
      // pane.setStyle("-fx-background-color:pink; -fx-padding: 20;");
       //pane.getStyleClass().add(MINE);
       pane.setAlignment(Pos.CENTER);
       pane.setHgap(6);
       pane.setVgap(6);
       save = new Button(props.getProperty(PropertyType.SAVE));
       cancel = new Button(props.getProperty(PropertyType.CANCEL));
       //label
       check =new Label(props.getProperty(PropertyType.CHECKBOX));
       Cat=new Label(props.getProperty(PropertyType.CATAGORY));
       Des = new Label(props.getProperty(PropertyType.DESCRIPTION)); 
       date = new Label(props.getProperty(PropertyType.START_DATE));
       date2 = new Label(props.getProperty(PropertyType.END_DATE));
       
       Des.setMinWidth(100);
       check.getStyleClass().add(CLASS_PROMPT_LABEL);
       Cat.getStyleClass().add(CLASS_PROMPT_LABEL);
       Des.getStyleClass().add(CLASS_PROMPT_LABEL);
       date.getStyleClass().add(CLASS_PROMPT_LABEL);
       date2.getStyleClass().add(CLASS_PROMPT_LABEL);
       
       
       ck = new CheckBox();
       start = new DatePicker();
       end = new DatePicker();
       cat = new TextField();
       des = new TextField();
       
       pane.add(Cat, 0, 0);
       pane.add(cat,1,0);
       pane.add(Des,0,1);
       pane.add(des,1,1);
       pane.add(date, 0, 2);
       pane.add(start,1,2);
       pane.add(date2,0,3);
       pane.add(end,1,3);
       pane.add(ck,1,4);
       pane.add(check,0,4);
       pane.add(save,0,5);
       pane.add(cancel,1,5);
       pane.getStyleClass().add(MINE);
 
       this.setScene(scene);
         save.setOnAction(e -> {
             items = new ToDoItem(cat.getText(),des.getText(),start.getValue(),end.getValue(),ck.isSelected());
            close();
            });
         cancel.setOnAction(e -> {
             close();
         });
        
      }
      public void Show(){
           showAndWait();
          }
      
      public void setCheck(boolean checked){
          ck.setSelected(checked);
      }
      public void setStartDate(LocalDate day){
          start.setValue(day);
      }
      public void setEndDate(LocalDate day){
         end.setValue(day);
      }
     public void setCat(String v){
         cat.setText(v);
     }
      public void setDes(String v){
          des.setText(v);
      }
      public ToDoItem getItem(){
          return items;
      }
      public void resetItem(){
          items=null;
      }
      
}
