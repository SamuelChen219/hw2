package mv.data;

import java.util.ArrayList;
import javafx.geometry.Point2D;
import saf.components.AppDataComponent;
import mv.MapViewerApp;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    ArrayList<ArrayList<Point2D>> polygon;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        polygon = new ArrayList<ArrayList<Point2D>>();
    }
    public void addPoly(ArrayList<Point2D> poly){
        polygon.add(poly);
    }
    public ArrayList<ArrayList<Point2D>> getPoly(){
        return polygon;
        
    }
    public void setPoint(int i, int j, Point2D point){
        polygon.get(j).set(i, point);
    }
    @Override
    public void reset() {
        polygon = new ArrayList<ArrayList<Point2D>>();
    }
}
