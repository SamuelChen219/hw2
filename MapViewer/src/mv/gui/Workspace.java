/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;


import java.net.URL;
import java.util.ArrayList;
import java.util.Stack;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;
import javafx.stage.Screen;
import javax.jws.soap.SOAPBinding;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.data.DataManager;
import properties_manager.PropertiesManager;
import saf.controller.AppFileController;
import static saf.settings.AppPropertyType.LOAD_ICON;
import static saf.settings.AppPropertyType.LOAD_TOOLTIP;
import static saf.settings.AppPropertyType.*;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    Button load;
    Button exit;
    FlowPane fileToolbarPane;
    Polygon poly;
    Pane polygonPane;
    Line equator;
    boolean pressed;
    double sceneW;
    double sceneH;
    ArrayList<Polygon> polygons = new ArrayList<Polygon>();
    ArrayList<Line> time = new ArrayList<Line>();
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        workspace = new Pane();
        sceneW = app.getGUI().getPrimaryScene().getWidth();
        sceneH = app.getGUI().getPrimaryScene().getHeight();
        polygonPane = new Pane(); 
        setupHandlers();
        
       
    }

    @Override
    public void reloadWorkspace() {
       DataManager data = (DataManager) app.getDataComponent();
       time.clear();
       polygonPane.getChildren().clear();
       polygonPane.setScaleX(1);
       polygonPane.setScaleY(1);
       polygonPane.setLayoutX(0);
       polygonPane.setLayoutY(0);
       polygonPane.setTranslateX(0);
       polygonPane.setTranslateY(0);
       pressed = false;
       setUp();
       data.reset();
    }

    @Override
    public void initStyle() {
        AppFileController fileController = new AppFileController(app);
        fileToolbarPane = new FlowPane();
         load = app.getGUI().initChildButton(fileToolbarPane,LOAD_ICON.toString(),LOAD_TOOLTIP.toString(), false);
         exit = app.getGUI().initChildButton(fileToolbarPane,EXIT_ICON.toString(),EXIT_TOOLTIP.toString(), false);
         fileToolbarPane.setStyle( "-fx-background-color: #aaaaff; -fx-background-radius: 5.0; -fx-padding: 20 ; -fx-spacing: 10; -fx-border-width: 2px; -fx-border-color: #7777dd;");
         app.getGUI().getAppPane().setTop(fileToolbarPane);
         load.setOnAction(e ->{
             fileController.handleLoadRequest();
         });
         exit.setOnAction(e ->{
             fileController.handleExitRequest();
         });
         load.setFocusTraversable(false);
         exit.setFocusTraversable(false);
         Rectangle clip = new Rectangle(sceneW,sceneH);
          workspace.setClip(clip);
         workspace.getChildren().add(polygonPane);
        app.getGUI().getAppPane().setCenter(workspace);
        
  
    }
    public void setUp(){
        workspace.setStyle("-fx-background-color: lightblue;");
           DataManager data = (DataManager) app.getDataComponent();
          ArrayList<ArrayList<Point2D>> polygon = data.getPoly();
           for(int j=0;j<polygon.size();j++){
              ArrayList<Point2D> points = polygon.get(j);
                poly = new Polygon();
           for(int i=0;i<points.size();i++){
                poly.setFill(Paint.valueOf("Green"));
                poly.setStroke(Paint.valueOf("Black"));
                poly.getPoints().addAll(points.get(i).getX(),points.get(i).getY());
              }
              polygonPane.getChildren().add(poly);
      }
          
           
           drawline();
           
    }
   
    public void setupHandlers(){
        
      polygonPane.setOnMouseClicked(e -> {
            if(e.getButton()== MouseButton.PRIMARY){
                double x=  e.getSceneX();
               double y = e.getSceneY();
              enlarge(x,y);
               
              }
            else {         
               double x=  e.getSceneX();
               double y = e.getSceneY();
               shrink(x,y);
             
            }
            
      });
      app.getGUI().getPrimaryScene().setOnKeyPressed(e ->{
           if(e.getCode()==KeyCode.RIGHT){
               moving(-10,0);
              
           }
           if(e.getCode()==KeyCode.LEFT){
               moving(10,0);
               
           }
           if(e.getCode()==KeyCode.UP){
              moving(0,10);
               
           }
           if(e.getCode()==KeyCode.DOWN){
               moving(0,-10);
             
           }
           if(e.getCode()==KeyCode.G){
               if(pressed == false){
                   pressed=true;
                  turnOn(time,pressed);
                   
               }
               else{
                    pressed = false;
                    turnOn(time,pressed);
               }
           }
       });
           
    }

    public void enlarge(double X, double Y) {
              polygonPane.setLayoutX((polygonPane.getLayoutX()+(sceneW/2-X))*2);
               polygonPane.setLayoutY((polygonPane.getLayoutY()+(sceneH/2-Y))*2);
               polygonPane.setScaleX(polygonPane.getScaleX()*2);
               polygonPane.setScaleY(polygonPane.getScaleY()*2);
             
     
 }

    public void shrink(double X, double Y) {
               
               polygonPane.setLayoutX((polygonPane.getLayoutX()+(sceneW/2-X))/2);
               polygonPane.setLayoutY((polygonPane.getLayoutY()+(sceneH/2-Y))/2);
               polygonPane.setScaleX(polygonPane.getScaleX()/2);
               polygonPane.setScaleY(polygonPane.getScaleY()/2);
          
    }

    public void moving(double X,double Y) {
     
                    polygonPane.setTranslateX(polygonPane.getTranslateX()+X);
                    polygonPane.setTranslateY(polygonPane.getTranslateY()+Y);
                
    }

    public void drawline() {
        for(int i = 0;i<=360;i+=30){
            double d = (i*sceneW)/360;
            if(i==180){
                  Line timeLine = new Line(d,0,d,sceneH-fileToolbarPane.getHeight());
                 timeLine.setStroke(Paint.valueOf("white"));
                 timeLine.setVisible(false);
                  polygonPane.getChildren().add(timeLine);
                  time.add(timeLine);
            }
            else{
            Line timeLine = new Line(d,0,d,sceneH-fileToolbarPane.getHeight());
            timeLine.setStroke(Paint.valueOf("white"));
            timeLine.getStrokeDashArray().add(5d);
            timeLine.setVisible(false);  
            polygonPane.getChildren().add(timeLine);
            time.add(timeLine);
            }
          }
        for(int i = 0;i<180;i+=30){
            double d = (i*(sceneH-fileToolbarPane.getHeight()))/180;
            if(i==90){
        Line timeLine = new Line(0,d,sceneW,d);
        timeLine.setStroke(Paint.valueOf("white"));
        timeLine.setVisible(false);
        polygonPane.getChildren().add(timeLine);
        time.add(timeLine);
          }
            else{
            Line timeLine = new Line(0,d,sceneW,d);
            timeLine.setStroke(Paint.valueOf("grey"));
            timeLine.getStrokeDashArray().add(5d);
            timeLine.setVisible(false);  
            polygonPane.getChildren().add(timeLine);
            time.add(timeLine);
            }
          }
         for(int i = 15;i<360;i+=15){
            double d = (i*sceneW)/360;
            if(i%30!=0){
             Line timeLine = new Line(d,0,d,sceneH-fileToolbarPane.getHeight()+3);
             timeLine.setStroke(Paint.valueOf("white"));
             timeLine.setVisible(false);
            polygonPane.getChildren().add(timeLine);
            time.add(timeLine);
            }
            
          }
        
    }
    public void turnOn(ArrayList<Line> time,boolean on){
        for(int i =0;i<time.size();i++){
            time.get(i).setVisible(on);
        }
    }
}
    

