/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
static final String JSON_SUBREGION = "SUBREGIONS";
static final String JSON_NUMBER_OF_REGION = "NUMBER_OF_SUBREGIONS";
static final String JSON_NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
static final String JSON_X = "X";
static final String JSON_Y = "Y";
ArrayList<Point2D> points ;
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
        JsonObject json = loadJSONFile(filePath);
        // num of region
        JsonArray SubRegion = json.getJsonArray(JSON_SUBREGION);
        // get the size of the screen
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        double Width = bounds.getWidth();
        double Height = bounds.getHeight()-75;
             //
        for(int i =0; i<SubRegion.size();i++){
           JsonObject Sub_Region_Poly = SubRegion.getJsonObject(i);
           int numOfPoly = getDataAsInt(Sub_Region_Poly,JSON_NUMBER_OF_SUBREGION_POLYGONS);
           JsonArray SubPolyRegion = Sub_Region_Poly.getJsonArray(JSON_SUBREGION_POLYGONS);
           //# of poly
           for (int j =0;j<SubPolyRegion.size();j++){
           JsonArray  coordinate = SubPolyRegion.getJsonArray(j); 
           points = new ArrayList<Point2D>();
           //XY
           for (int k = 0;k<coordinate.size() ;k++){
               JsonObject coor = coordinate.getJsonObject(k);
               double X = getDataAsDouble(coor,JSON_X);
               double Y = getDataAsDouble(coor,JSON_Y);
               X+=180;
               Y=Y*-1+90;
               X=(X/360.0)*Width;
               Y=(Y/180.0)*Height;
               Point2D point = new Point2D(X,Y);
               points.add(point);
              }
           dataManager.addPoly(points);
          }
        }
        
       }
     public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
